package id.bootcamp.crudperpustakaan.di

import android.content.Context
import id.bootcamp.crudperpustakaan.data.MyRepository
import id.bootcamp.crudperpustakaan.data.local.room.MyDatabase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

object Injection {
    fun provideExampleRepository(context: Context): MyRepository{
        val myDatabase = MyDatabase.getInstance(context)
        val localDao = myDatabase.localDao()
        GlobalScope.launch { localDao.getLocalUser() }
        return MyRepository.getInstance(localDao)
    }
}