package id.bootcamp.crudperpustakaan.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import id.bootcamp.crudperpustakaan.databinding.FragmentDeleteBinding
import id.bootcamp.crudperpustakaan.ui.viewmodel.ExampleViewModel
import id.bootcamp.crudperpustakaan.ui.viewmodel.MyViewModelFactory

class DeleteBookConfirmationFragment : DialogFragment() {
    private lateinit var binding: FragmentDeleteBinding
    private lateinit var viewModel: ExampleViewModel
    private var bookId = 0L
    var onDeleteListener: OnDeleteListener? = null
    interface OnDeleteListener { fun onDeleteSuccess() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        // Beda bindingnya
        binding = FragmentDeleteBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this,MyViewModelFactory
            .getInstance(requireContext()))[ExampleViewModel::class.java]
        //require context kalau di fragment

        bookId = arguments?.getLong("bookId") ?: 0

        viewModel.getBookById(bookId)

        viewModel.bookDetailLiveData.observe(this) {
            if (it != null) {
                binding.tvAuthor.text = it.author
                binding.tvTitle.text = it.title
            }
        }

        setupListener()
    }

    private fun setupListener() {
        binding.bCRUD.setOnClickListener {
            onDeleteListener?.onDeleteSuccess()
            viewModel.deleteBook(bookId)
            dialog?.dismiss()
        }
        binding.bCancel.setOnClickListener {
            dialog?.dismiss()
        }
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT)
    }
}