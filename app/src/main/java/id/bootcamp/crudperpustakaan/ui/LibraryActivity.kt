package id.bootcamp.crudperpustakaan.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.bootcamp.crudperpustakaan.data.local.entity.LibraryEntity
import id.bootcamp.crudperpustakaan.databinding.ActivityLibraryBinding
import id.bootcamp.crudperpustakaan.ui.viewmodel.ExampleViewModel
import id.bootcamp.crudperpustakaan.ui.viewmodel.MyViewModelFactory
import id.bootcamp.crudperpustakaan.ui.adapter.BookListAdapter

class LibraryActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLibraryBinding
    private lateinit var viewModel: ExampleViewModel
    private lateinit var bAdapter: BookListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLibraryBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //Inisiasi Viewmodel
        viewModel = ViewModelProvider(
            this, MyViewModelFactory.getInstance(this)
        )[ExampleViewModel::class.java]

        supportActionBar?.title = "CRUD Library"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        viewModel.getAllBooks()

        viewModel.bookListLiveData.observe(this) {
            if (it != null) {
                setRecyclerView(ArrayList(it))
                viewModel.bookListLiveData.postValue(null)
            }
        }

        binding.fabAddBook.setOnClickListener {
            val intent = Intent(this,AddBookActivity::class.java)
            intent.putExtra("state","add")
            resultLauncher.launch(intent)
        }
    }

    private fun setRecyclerView(al : ArrayList<LibraryEntity>) {
        binding.rvBookList.layoutManager = LinearLayoutManager(this)
        bAdapter = BookListAdapter(al)
        binding.rvBookList.adapter = bAdapter

        bAdapter.setOnItemListener(object: BookListAdapter.OnItemListener{
            override fun onItemClick(bookId: Long) {
                val intent = Intent(this@LibraryActivity,DetailActivity::class.java)
                intent.putExtra("bookId",bookId)
                resultLauncher.launch(intent)
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    var resultLauncher = registerForActivityResult(
        ActivityResultContracts
            .StartActivityForResult()
    ) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            viewModel.getAllBooks()
        }
    }
}