package id.bootcamp.crudperpustakaan.ui

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import id.bootcamp.crudperpustakaan.R
import id.bootcamp.crudperpustakaan.databinding.ActivityBookDetailBinding
import id.bootcamp.crudperpustakaan.ui.viewmodel.ExampleViewModel
import id.bootcamp.crudperpustakaan.ui.viewmodel.MyViewModelFactory

class DetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityBookDetailBinding
    private lateinit var viewModel: ExampleViewModel
    private var bookId = 0L
    private var isUpdated = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBookDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(this,MyViewModelFactory
            .getInstance(this))[ExampleViewModel::class.java]

        supportActionBar?.title = "Book Detail"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        bookId = intent.getLongExtra("bookId",0L)

        viewModel.getBookById(bookId)

        viewModel.bookDetailLiveData.observe(this) {
            if (it != null) {
                if (Patterns.WEB_URL.matcher(it.coverPath).matches()) {
                    Glide.with(this).load(it.coverPath).into(binding.ivCover)
                }
                binding.tvTitle.text = it.title
                binding.tvAuthor.text = it.author
                val count = it.total ?: 0
                if (it.isAvailable && count > 0) {
                    binding.tvBookCount.text = count.toString()
                    binding.llBooksAvailable.visibility = View.VISIBLE
                    binding.tvBooksNotAvailable.visibility = View.GONE
                }
                else {
                    binding.llBooksAvailable.visibility = View.GONE
                    binding.tvBooksNotAvailable.visibility = View.VISIBLE
                }
                binding.tvBookId.text = "Book ID: $bookId"
            }
        }
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
            R.id.mEdit -> {
                val intent = Intent(this,AddBookActivity::class.java)
                intent.putExtra("state","edit")
                intent.putExtra("bookId",bookId)
                resultLauncher.launch(intent)
            }
            R.id.mDelete -> {
                val bundle = Bundle()
                bundle.putLong("bookId",bookId)

                val deleteFragment = DeleteBookConfirmationFragment()
                deleteFragment.arguments = bundle
                val fragmentManager = supportFragmentManager

                deleteFragment.show(fragmentManager,"delete")

                deleteFragment.onDeleteListener = object
                    : DeleteBookConfirmationFragment.OnDeleteListener{
                    override fun onDeleteSuccess() {
                        setResult(RESULT_OK, Intent())
                        finish()
                    }
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    var resultLauncher = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            bookId = result.data?.getLongExtra("bookId",0L) ?: 0L
            isUpdated = result.data?.getBooleanExtra("isUpdated",false) ?: false
            viewModel.getBookById(bookId)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.update_delete_menu,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onBackPressed() {
        if (isUpdated) {
            setResult(RESULT_OK, Intent())
            finish()
        }
        super.onBackPressed()
    }
}