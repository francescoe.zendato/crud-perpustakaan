package id.bootcamp.crudperpustakaan.ui

import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import id.bootcamp.crudperpustakaan.R
import id.bootcamp.crudperpustakaan.databinding.ActivityCreateUpdateBookBinding
import id.bootcamp.crudperpustakaan.ui.viewmodel.ExampleViewModel
import id.bootcamp.crudperpustakaan.ui.viewmodel.MyViewModelFactory
import id.bootcamp.crudperpustakaan.ui.data.BookData

class AddBookActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCreateUpdateBookBinding
    private lateinit var viewModel: ExampleViewModel
    private var state = "add"
    private var bookId = 0L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCreateUpdateBookBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(
            this, MyViewModelFactory.getInstance(this)
        )[ExampleViewModel::class.java]

        state = intent?.getStringExtra("state") ?: "add"

        when (state) {
            "edit" -> {
                bookId = intent.getLongExtra("bookId",0L)
                binding.bCRUD.text = "Save Book"
                binding.tvInputEdit.text = "Edit Book Data"

                viewModel.getBookById(bookId)
            }
        }

        viewModel.bookDetailLiveData.observe(this) {
            if (it != null) {
                binding.etTitle.setText(it.title)
                binding.etAuthor.setText(it.author)
                if (!it.isAvailable) {
                    binding.llNumberOfBooks.visibility = View.GONE
                    binding.rbYes.isChecked = false
                    binding.rbNo.isChecked = true
                }
                if (it.total != null) binding.etNumberOfBooks.setText(it.total.toString())
                binding.etCoverPath.setText(it.coverPath)
                viewModel.bookDetailLiveData.postValue(null)
            }
        }

        setupListener()
    }

    private fun clearInput() {
        binding.etAuthor.setText(null)
        binding.etTitle.setText(null)
        binding.etCoverPath.setText(null)
        binding.rgAvailability.clearCheck()
        binding.rbYes.isChecked = true
        binding.llNumberOfBooks.visibility = View.VISIBLE
        binding.etNumberOfBooks.setText(null)
    }
    private fun setupListener() {
        binding.bReset.setOnClickListener {
            clearInput()
        }
        binding.bCRUD.setOnClickListener {
            val total = binding.etNumberOfBooks.text.toString().trim()
            if (binding.rbYes.isChecked && (total == "" || total.toInt() == 0)) {
                binding.etNumberOfBooks.error = "Must be > 0"
                return@setOnClickListener
            }
            val data = BookData()
            data.author = binding.etAuthor.text.toString().trim()
            data.title = binding.etTitle.text.toString().trim()
            data.coverPath = binding.etCoverPath.text.toString().trim()
            data.isAvailable = binding.rbYes.isChecked
            if (data.isAvailable) data.total = total.toInt()
            when (state) {
                "add" -> {
                    viewModel.insertBook(data)
                    setResult(RESULT_OK, Intent())
                    finish()
                }
                "edit" -> {
                    viewModel.updateBook(data, bookId)
                    val intent = Intent()
                    intent.putExtra("bookId",bookId)
                    intent.putExtra("isUpdated",true)
                    setResult(RESULT_OK, intent)
                    finish()
                }
            }
        }
        binding.bCancel.setOnClickListener {onBackPressed()}
        binding.rgAvailability.setOnCheckedChangeListener { rg, id ->
            when (id) {
                R.id.rbYes -> binding.llNumberOfBooks.visibility = View.VISIBLE
                R.id.rbNo -> binding.llNumberOfBooks.visibility = View.GONE
            }
        }
    }
}