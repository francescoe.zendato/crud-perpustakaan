package id.bootcamp.crudperpustakaan.ui.adapter

import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import id.bootcamp.crudperpustakaan.R
import id.bootcamp.crudperpustakaan.data.local.entity.LibraryEntity
import id.bootcamp.crudperpustakaan.ui.LibraryActivity
import id.bootcamp.crudperpustakaan.ui.data.BookData

class BookListAdapter(val bookList: ArrayList<LibraryEntity>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    interface OnItemListener {
        fun onItemClick(bookId: Long)
    }

    private var onItemListener : OnItemListener? = null
    fun setOnItemListener(onItemListener: OnItemListener) {
        this.onItemListener = onItemListener
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val cover = itemView.findViewById<ImageView>(R.id.ivCover)
        val title = itemView.findViewById<TextView>(R.id.tvTitle)
        val author = itemView.findViewById<TextView>(R.id.tvAuthor)
        val llAvailable = itemView.findViewById<LinearLayout>(R.id.llBooksAvailable)
        val total = itemView.findViewById<TextView>(R.id.tvBookCount)
        val tvNotAvailable = itemView.findViewById<TextView>(R.id.tvBooksNotAvailable)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_book_preview,parent,
                false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return bookList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder) {
            val data = bookList[position]
            if (Patterns.WEB_URL.matcher(data.coverPath).matches()) {
                Glide.with(holder.itemView).load(data.coverPath).into(holder.cover)
            }
            holder.title.text = data.title
            holder.author.text = data.author
            val bookCount = data.total ?: 0
            if (data.isAvailable && bookCount > 0) holder.total.text = bookCount.toString()
            else {
                holder.llAvailable.visibility = View.GONE
                holder.tvNotAvailable.visibility = View.VISIBLE
            }

            if (onItemListener != null) {
                holder.itemView.setOnClickListener {
                    onItemListener?.onItemClick(data.id)
                }
            }
        }
    }
}