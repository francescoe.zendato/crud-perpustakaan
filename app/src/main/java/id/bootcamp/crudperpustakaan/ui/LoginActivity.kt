package id.bootcamp.crudperpustakaan.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import id.bootcamp.crudperpustakaan.R
import id.bootcamp.crudperpustakaan.databinding.ActivityLoginBinding
import id.bootcamp.crudperpustakaan.ui.adapter.BookListAdapter
import id.bootcamp.crudperpustakaan.ui.viewmodel.ExampleViewModel
import id.bootcamp.crudperpustakaan.ui.viewmodel.MyViewModelFactory

class LoginActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding
    private lateinit var viewModel: ExampleViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(this,
            MyViewModelFactory.getInstance(this))[ExampleViewModel::class.java]
    }
}