package id.bootcamp.crudperpustakaan.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bootcamp.crudperpustakaan.data.MyRepository
import id.bootcamp.crudperpustakaan.data.local.entity.LibraryEntity
import id.bootcamp.crudperpustakaan.data.local.entity.LocalEntity
import id.bootcamp.crudperpustakaan.ui.data.BookData
import kotlinx.coroutines.launch

class ExampleViewModel(private val myRepository: MyRepository) : ViewModel() {
    val userLiveData = MutableLiveData<LocalEntity>()
    val bookListLiveData = MutableLiveData<List<LibraryEntity>>()
    val bookDetailLiveData = MutableLiveData<LibraryEntity>()

    fun getUser() = viewModelScope.launch {
        val user = myRepository.getLocalUser()
        userLiveData.postValue(user ?: LocalEntity())
    }

    fun insertBook(bookData: BookData) = viewModelScope.launch {
        myRepository.insertBook(bookData)
    }

    fun getAllBooks() = viewModelScope.launch {
        val list = myRepository.getAllBooks()
        bookListLiveData.postValue(list)
    }

    fun getBookById(id: Long) = viewModelScope.launch {
        val data = myRepository.getBookById(id)
        bookDetailLiveData.postValue(data)
    }

    fun updateBook(bookData: BookData, bookId: Long) = viewModelScope.launch {
        myRepository.updateBook(bookData, bookId)
    }

    fun deleteBook(bookId: Long) = viewModelScope.launch {
        myRepository.deleteBook(bookId)
    }
}