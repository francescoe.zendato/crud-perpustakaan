package id.bootcamp.crudperpustakaan.ui.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class BookData(
    var title: String = "",
    var author: String? = null,
    var total: Int? = null,
    var coverPath: String? = null,
    var isAvailable: Boolean = true
) : Parcelable
