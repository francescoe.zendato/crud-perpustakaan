package id.bootcamp.crudperpustakaan.data.local.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import id.bootcamp.crudperpustakaan.data.local.entity.LibraryEntity
import id.bootcamp.crudperpustakaan.data.local.entity.LocalEntity

@TypeConverters(DateConverter::class)
@Database(entities = [LocalEntity::class, LibraryEntity::class], version = 2)
abstract class MyDatabase : RoomDatabase() {

    abstract fun localDao(): LocalDao

    companion object {
        @Volatile
        private var instance: MyDatabase? = null
        fun getInstance(context: Context): MyDatabase =
            instance ?: synchronized(this) {
                instance ?: Room.databaseBuilder(
                    context.applicationContext,
                    MyDatabase::class.java, "my-database.db"
                )
                    //.createFromAsset("database/example.db")
                    //.addMigrations(migrations)
                    .fallbackToDestructiveMigration()
                    .build()
            }

        val migrations = object : Migration(2,3) {
            override fun migrate(db: SupportSQLiteDatabase) {
            }
        }
    }
}