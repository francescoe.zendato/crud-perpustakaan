package id.bootcamp.crudperpustakaan.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user")
class LocalEntity(

    @field:ColumnInfo(name = "id")
    @field:PrimaryKey
    var id: Int = -1,

    @field:ColumnInfo(name = "fullname")
    var fullname: String = "",
)