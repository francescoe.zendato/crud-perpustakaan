package id.bootcamp.crudperpustakaan.data.local.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import id.bootcamp.crudperpustakaan.data.local.entity.LibraryEntity
import id.bootcamp.crudperpustakaan.data.local.entity.LocalEntity

@Dao
interface LocalDao {
    @Query("SELECT * FROM user")
    suspend fun getLocalUser(): LocalEntity?

    @Insert
    suspend fun insertBook(libraryEntity: LibraryEntity)

    @Update
    suspend fun updateBook(libraryEntity: LibraryEntity)

    @Query("SELECT * FROM library where is_delete = 0")
    suspend fun getAllBooks() : List<LibraryEntity>
    @Query("SELECT * FROM library Where id = :id and is_delete = 0")
    suspend fun getBookById(id: Long): LibraryEntity
}