package id.bootcamp.crudperpustakaan.data

import id.bootcamp.crudperpustakaan.data.local.entity.LibraryEntity
import id.bootcamp.crudperpustakaan.data.local.entity.LocalEntity
import id.bootcamp.crudperpustakaan.data.local.room.LocalDao
import id.bootcamp.crudperpustakaan.ui.data.BookData
import java.util.Date

class MyRepository private constructor(
    private val localDao: LocalDao,
) {
    suspend fun getLocalUser() : LocalEntity? {
       return localDao.getLocalUser()
    }
    suspend fun insertBook(bookData: BookData) {
        val book = LibraryEntity()
        book.author = bookData.author
        book.title = bookData.title
        book.total = bookData.total
        book.isAvailable = bookData.isAvailable
        book.coverPath = bookData.coverPath
        book.createdBy = 1
        book.createdOn = Date()
        book.isDelete = false

        return localDao.insertBook(book)
    }

    suspend fun updateBook(bookData: BookData, bookId: Long) {
        val book = localDao.getBookById(bookId)
        book.author = bookData.author
        book.title = bookData.title
        book.total = bookData.total
        book.isAvailable = bookData.isAvailable
        book.coverPath = bookData.coverPath
        book.modifiedBy = 1
        book.modifiedOn = Date()

        localDao.updateBook(book)
    }

    suspend fun getAllBooks() : List<LibraryEntity> {
        return localDao.getAllBooks()
    }

    suspend fun getBookById(bookId: Long): LibraryEntity {
        return localDao.getBookById(bookId)
    }

    suspend fun deleteBook(bookId: Long) {
        val book = localDao.getBookById(bookId)
        book.deletedBy = 1
        book.deleteOn = Date()
        book.isDelete = true
        localDao.updateBook(book)
    }

    companion object {
        @Volatile
        private var instance: MyRepository? = null
        fun getInstance(
            localDao: LocalDao,
        ): MyRepository =
            instance ?: synchronized(this) {
                instance ?: MyRepository(localDao)
            }.also { instance = it }
    }
}