package id.bootcamp.crudperpustakaan.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.Date

@Entity(tableName = "library")
class LibraryEntity() {
    @field:ColumnInfo(name = "id")
    @field:PrimaryKey(autoGenerate = true)
    var id: Long = 0

    @field:ColumnInfo(name = "title")
    var title: String = ""

    @field:ColumnInfo(name = "author")
    var author: String? = null

    @field:ColumnInfo(name = "total")
    var total: Int? = null

    @field:ColumnInfo(name = "is_available")
    var isAvailable: Boolean = true

    @field:ColumnInfo(name = "cover_path")
    var coverPath: String? = null

    @field:ColumnInfo(name = "created_by")
    var createdBy: Long = 0

    @field:ColumnInfo(name = "created_on")
    var createdOn: Date? = Date()

    @field:ColumnInfo(name = "modified_by")
    var modifiedBy: Long? = 0

    @field:ColumnInfo(name = "modified_on")
    var modifiedOn: Date? = Date()

    @field:ColumnInfo(name = "deleted_by")
    var deletedBy: Long? = 0

    @field:ColumnInfo(name = "deleted_on")
    var deleteOn: Date? = Date()

    @field:ColumnInfo(name = "is_delete")
    var isDelete: Boolean = false
}